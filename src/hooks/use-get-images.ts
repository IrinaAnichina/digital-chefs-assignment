import { QueryFunctionContext, useInfiniteQuery } from 'react-query';
import axiosClient from '../axios';
import { ImageData } from '../types';

const fetchImages = async ({
  pageParam,
}: QueryFunctionContext<'images', number>): Promise<ImageData[]> => {
  const response = await axiosClient.get<ImageData[]>(`photos`, {
    params: {
      page: pageParam || 1,
    },
  });
  return response.data;
};

export const useGetImages = () =>
  useInfiniteQuery({
    queryKey: 'images',
    queryFn: fetchImages,
    getNextPageParam: (lastPage, allPages) => {
      if (lastPage.length === 0) {
        return undefined;
      }
      return allPages.length + 1;
    },
    refetchOnWindowFocus: false,
  });
