import { QueryFunctionContext, useInfiniteQuery } from 'react-query';
import axiosClient from '../axios';
import { ImageData, SearchParams } from '../types';

const searchImages = async ({
  pageParam,
  query,
  color,
  orientation,
}: SearchParams & { pageParam?: number }): Promise<ImageData[]> => {
  const response = await axiosClient.get(`search/photos`, {
    params: {
      page: pageParam || 1,
      query,
      color,
      orientation,
    },
  });
  return response.data.results;
};

export const useSearchImages = ({
  query,
  color,
  orientation,
}: SearchParams) => {
  const queryKey = ['search', query, color, orientation];
  return useInfiniteQuery({
    queryKey,
    queryFn: ({ pageParam }: QueryFunctionContext<typeof queryKey, number>) =>
      searchImages({ query, color, orientation, pageParam }),
    getNextPageParam: (lastPage, allPages) => {
      if (lastPage.length === 0) {
        return undefined;
      }
      return allPages.length + 1;
    },
    refetchOnWindowFocus: false,
    enabled: !!queryKey,
  });
};
