import { createContext } from 'react';
import { SearchParams } from '../types';

export const SearchContext = createContext<SearchParams>({});
