import { MantineProvider } from '@mantine/core';
import { QueryClient, QueryClientProvider } from 'react-query';

import '@mantine/core/styles.css';

import { mantineTheme } from './theme';
import { AppShell } from './components/app-shell';

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <MantineProvider theme={mantineTheme}>
        <AppShell />
      </MantineProvider>
    </QueryClientProvider>
  );
}

export default App;
