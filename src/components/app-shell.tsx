import { useEffect, useState } from 'react';
import {
  AppShell as MantineAppShell,
  Group,
  Image,
  CloseButton,
  Input,
  Stack,
  ActionIcon,
} from '@mantine/core';
import { useDisclosure, useFocusWithin, useMediaQuery } from '@mantine/hooks';
import {
  IconFilter,
  IconFilterFilled,
  IconSearch,
  IconX,
} from '@tabler/icons-react';

import logo from '../assets/Unsplash_Symbol.svg';
import { SearchContext } from '../context/search-context';
import { SearchParams, UColor, UColors } from '../types';
import { ImageGrid } from './image-grid';
import { ColorSwatch } from './color-swatch';

export const AppShell = () => {
  const { ref, focused } = useFocusWithin();
  const [value, setValue] = useState('');
  const [searchParams, setSearchParams] = useState<SearchParams>({});

  const isMobile = useMediaQuery(`(max-width: 48em`);
  const [filterOpened, { toggle: toggleFilters, close: closeFilters }] =
    useDisclosure();

  const setColor = (color?: UColor) => {
    setSearchParams(current => {
      if (current.color === color) return { ...current, color: undefined };
      return { ...current, color };
    });
    if (isMobile) closeFilters();
  };

  const clearSearch = () => {
    setValue('');
    closeFilters();
    setSearchParams({});
    ref.current.focus();
  };

  useEffect(() => {
    const updateValue = () =>
      setSearchParams(current => ({ ...current, query: value }));
    if (!focused) {
      updateValue();
      return;
    }
    const updateSearch = setTimeout(updateValue, 800);
    return () => clearTimeout(updateSearch);
  }, [value, focused]);

  return (
    <SearchContext.Provider value={searchParams}>
      <MantineAppShell
        padding='md'
        styles={{ main: { height: '100vh' } }}
        header={{ height: 80 }}
        aside={{
          width: 70,
          breakpoint: 'sm',
          collapsed: { mobile: !filterOpened, desktop: !filterOpened },
        }}
      >
        <MantineAppShell.Header p='20'>
          <Group justify='stretch' align='start' gap={40}>
            <Image src={logo} h='40' w='40' />
            <Stack style={{ flex: 1 }}>
              <Input
                ref={ref}
                w='100%'
                autoFocus
                placeholder='Search image'
                leftSection={<IconSearch size={16} />}
                value={value}
                onChange={event => setValue(event.currentTarget.value)}
                onKeyUp={event => {
                  if (event.key === 'Enter') event.currentTarget.blur();
                }}
                rightSectionPointerEvents='all'
                rightSection={
                  <CloseButton
                    aria-label='Clear search'
                    onClick={clearSearch}
                    style={{ display: value ? undefined : 'none' }}
                  />
                }
              />
            </Stack>
            <ActionIcon
              variant='subtle'
              onClick={toggleFilters}
              disabled={!searchParams.query}
              color='dark.4'
              style={{ backgroundColor: 'transparent' }}
            >
              {searchParams.color ? <IconFilterFilled /> : <IconFilter />}
            </ActionIcon>
          </Group>
        </MantineAppShell.Header>

        <MantineAppShell.Main>
          <ImageGrid />
        </MantineAppShell.Main>
        {filterOpened && (
          <MantineAppShell.Aside p={20}>
            <Group gap={8}>
              {UColors.map(color => (
                <ColorSwatch
                  key={color}
                  color={color}
                  setColor={setColor}
                  checked={color === searchParams.color}
                />
              ))}
              {searchParams.color && (
                <ActionIcon
                  onClick={() => setColor(undefined)}
                  variant='subtle'
                  color='dark'
                >
                  <IconX />
                </ActionIcon>
              )}
            </Group>
          </MantineAppShell.Aside>
        )}
      </MantineAppShell>
    </SearchContext.Provider>
  );
};
