import { ColorSwatch as MantineColorSwatch } from '@mantine/core';
import { IconCheck } from '@tabler/icons-react';
import { UColor } from '../types';

type Props = {
  color: UColor;
  setColor: (color: UColor) => void;
  checked?: boolean;
};

export const ColorSwatch: React.FC<Props> = ({ color, setColor, checked }) => {
  const isLight = (color: UColor): boolean =>
    color.endsWith('white') || color === 'yellow';
  const bwStyles = {
    styles: {
      root: {
        backgroundImage:
          'conic-gradient(black 0deg, black 180deg, white 180deg)',
      },
      colorOverlay: {
        display: 'none',
      },
      alphaOverlay: {
        display: 'none',
      },
    },
  };
  return (
    <MantineColorSwatch
      component='button'
      onClick={() => setColor(color)}
      color={color === 'black_and_white' ? 'gray' : color}
      variant='filled'
      {...(color === 'black_and_white' && bwStyles)}
    >
      {checked && (
        <IconCheck color={isLight(color) ? '#aaa' : 'white'} stroke={3} />
      )}
    </MantineColorSwatch>
  );
};
