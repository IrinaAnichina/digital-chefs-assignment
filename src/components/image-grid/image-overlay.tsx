import { useEffect } from 'react';
import {
  CloseButton,
  Image,
  Overlay,
  Portal,
  Stack,
  Text,
} from '@mantine/core';
import { ImageData } from '../../types';

type Props = {
  image: ImageData;
  onClose: () => void;
};

export const ImageOverlay: React.FC<Props> = ({ image, onClose }) => {
  const {
    urls,
    user: { first_name, last_name },
  } = image;
  const name = [first_name, last_name].filter(n => !!n).join(' ') || '';

  useEffect(() => {
    const keyDownHandler = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        onClose();
      }
    };

    document.addEventListener('keydown', keyDownHandler);

    return () => {
      document.removeEventListener('keydown', keyDownHandler);
    };
  }, [onClose]);

  return (
    <Portal>
      <Overlay onClick={onClose} center fixed backgroundOpacity={0.8}>
        <Stack p={20} h='100%' justify='center' align='end' gap={8}>
          <Image src={urls.regular} fit='contain' mah='95%' />
          <Text c='white'>{name}</Text>
        </Stack>
        <CloseButton
          onClick={onClose}
          variant='transparent'
          size='xl'
          pos='fixed'
          top={20}
          right={20}
          style={{ color: 'white' }}
        />
      </Overlay>
    </Portal>
  );
};
