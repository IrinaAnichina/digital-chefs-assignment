import { useRef, useState } from 'react';
import { Card, Center, Group, Image, ScrollArea, Stack } from '@mantine/core';
import { useMediaQuery } from '@mantine/hooks';
import { IconCloudSearch } from '@tabler/icons-react';

import { ImageData } from '../../types';
import { ImageOverlay } from './image-overlay';

type Props = { images: ImageData[]; loadMore: () => void };

export const ImageGrid: React.FC<Props> = ({ images, loadMore }) => {
  const viewport = useRef<HTMLDivElement>(null);
  const [viewedImage, setViewedImage] = useState<ImageData>();
  const isMd = useMediaQuery(`(min-width: 48em`);
  const isLg = useMediaQuery('(min-width: 62em)');
  const columns = isLg ? 3 : isMd ? 2 : 1;

  const imageColumns = new Array<ImageData[]>(columns);

  for (let i = 0; i < columns; i++) {
    imageColumns[i] = images.filter((_, index) => index % columns === i);
  }

  const onScrollPositionChange = (position: { x: number; y: number }) => {
    if (
      viewport.current &&
      position.y ===
        viewport.current.scrollHeight - viewport.current.clientHeight
    ) {
      loadMore();
    }
  };

  if (images.length === 0)
    return (
      <Center style={{ color: 'gray' }} p={40}>
        <IconCloudSearch size='48' />
        Nothing found
      </Center>
    );

  return (
    <>
      <ScrollArea
        h='100%'
        viewportRef={viewport}
        onScrollPositionChange={onScrollPositionChange}
        type='hover'
        offsetScrollbars
      >
        <Group gap={16} align='start' justify='stretch'>
          {imageColumns.map((col, index) => (
            <Stack key={index} gap={16} style={{ flex: 1 }}>
              {col.map((image, imageIndex) => (
                <Card
                  key={image.id}
                  style={{ cursor: 'pointer' }}
                  onClick={() => setViewedImage(image)}
                  onKeyUp={event => {
                    if (event.key === 'Enter') {
                      event.stopPropagation();
                      setViewedImage(image);
                    }
                  }}
                  component='a'
                  tabIndex={imageIndex * col.length + index + 1}
                >
                  <Card.Section>
                    <Image
                      src={image.urls.regular}
                      alt={image.description}
                      w='100%'
                    />
                  </Card.Section>
                </Card>
              ))}
            </Stack>
          ))}
        </Group>
      </ScrollArea>
      {!!viewedImage && (
        <ImageOverlay
          image={viewedImage}
          onClose={() => setViewedImage(undefined)}
        />
      )}
    </>
  );
};
