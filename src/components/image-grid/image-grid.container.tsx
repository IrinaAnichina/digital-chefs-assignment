import { useContext } from 'react';
import { Center, Loader } from '@mantine/core';
import { useGetImages, useSearchImages } from '../../hooks';
import { SearchContext } from '../../context/search-context';
import { ImageGrid } from './image-grid';

export const ImageGridContainer = () => {
  const { query, color, orientation } = useContext(SearchContext);
  const useSearch = !!query;
  const { data: allData, fetchNextPage, isFetching } = useGetImages();
  const {
    data: searchData,
    fetchNextPage: fetchNextSearchPage,
    isFetching: isSearchFetching,
  } = useSearchImages({ query, color, orientation });
  const data = (useSearch ? searchData : allData)?.pages.flatMap(e => e);

  const loadMore = () => {
    if (query) {
      if (!isSearchFetching) fetchNextSearchPage();
    } else {
      if (!isFetching) fetchNextPage();
    }
  };

  if (!data)
    return (
      <Center h='100%' p={20}>
        <Loader color='gray' />
      </Center>
    );

  return <ImageGrid images={data} loadMore={loadMore} />;
};
