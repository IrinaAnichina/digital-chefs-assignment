export type ImageData = {
  id: string;
  height: number;
  width: number;
  description: string;
  urls: {
    thumb: string;
    small: string;
    regular: string;
  };
  user: {
    first_name: string | null;
    last_name: string | null;
  };
};

export const UColors = [
  'yellow',
  'orange',
  'red',
  'purple',
  'magenta',
  'green',
  'teal',
  'blue',
  'black',
  'white',
  'black_and_white',
];

export type UColor = (typeof UColors)[number];

export type SearchParams = {
  query?: string;
  color?: UColor;
  orientation?: 'landscape' | 'portrait' | 'squarish';
};
